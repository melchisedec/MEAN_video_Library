var express = require("express");
var app = express();
var port = process.env.PORT || 8888;

var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require("morgan");
var mongoose = require("mongoose");
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');

var configDB = require("./config/database.js");
mongoose.connect(configDB.url);
require('./config/passport')(passport);
app.use("/css", express.static(__dirname + "/css"));

app.use(morgan('dev'));

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
    secret: 'anystringtext',
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.set('view engine', 'ejs');

/*
app.use('/', function(request, response) {
    response.send('Our First Express Program255');
    console.log(request.cookies);
    console.log('====================');
    console.log(request.session);
});
*/

require('./app/routes.js')(app, passport);


app.listen(port);
console.log("Server RUNNINGg on port:" + port);
// cookie-parser looks at the headers parsed between the client and server, reads them and passes the cookies that are being sent
// saves them in request.cookies

//express-session allows us to authenticate transactions between the client and the server
// to make sure its the same person
// add them to your package.json file and install them