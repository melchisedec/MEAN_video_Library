var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var User = require("../app/models/user");


var configAuth = require("./auth");
global.tokenFBVMUser = {};
module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        //

        function(request, email, password, done) {
            process.nextTick(function() {
                User.findOne({ 'local.username': email },
                    function(err, user) {
                        if (err)
                            return done(err);
                        if (user) {
                            return done(null, false, request.flash('signupmessage', 'email already taken'));
                        } else {
                            var newUser = new User();
                            newUser.local.username = email;
                            newUser.local.password = newUser.local.generateHash(password);
                            newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            })
                        }
                    })
            });
        }));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(request, email, password, done) {
            process.nextTick(function() {
                User.findOne({ 'local.username': email },
                    function(err, user) {
                        if (err) return done(err);
                        if (!user) return done(null, false, request.flash('loginmessage', 'No user Found'));
                        // if (user.local.password != password) return done(null, false, request.flash('loginmessage', 'Invalid password'));
                        if (!user.validpassword(password)) return done(null, false, request.flash('loginmessage', 'Invalid password'));
                        return done(null, user);
                    })
            })
        }
    ));
    /////////


    passport.use(new FacebookStrategy({
        clientID: configAuth.facebookAuth.ClientID,
        clientSecret: configAuth.facebookAuth.ClientSecret,
        callbackURL: configAuth.facebookAuth.callbackURL,

        profileFields: ['id', 'displayName', 'emails', 'first_name', 'middle_name', 'last_name'],

        enableProof: true
    }, function(accessToken, refreshToken, profile, done) {

        process.nextTick(function() {
            User.findOne({
                    'facebook.id': profile.id
                },
                function(err, user) {
                    global.tokenFBVMUser[profile.id] = accessToken;
                    if (err) {
                        return done(err);
                    }
                    if (user) {
                        return done(null, user);
                    } else {
                        var newUser = new User();
                        newUser.facebook.id = profile.id;
                        newUser.facebook.accessToken = accessToken;
                        newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
                        newUser.facebook.email = profile.emails[0].value;

                        newUser.save(function(err) {
                            if (err) {
                                throw err;
                            }
                            return done(null, newUser);
                        });

                    }
                });
        });
    }));

    ///////////////////////////

    passport.use(new GoogleStrategy({
            clientID: configAuth.googleAuth.ClientID,
            clientSecret: configAuth.googleAuth.ClientSecret,
            callbackURL: configAuth.googleAuth.callbackURL
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function() {
                User.findOne({ 'google.id': profile.id },
                    function(err, user) {
                        if (err) return done(err);
                        if (user) return done(null, user);
                        else {
                            var newUser = new User();
                            newUser.google.id = profile.id;
                            newUser.google.token = profile.token;
                            newUser.google.name = profile.name.displayName;
                            newUser.google.email = profile.email;
                            newUser.save(function(err) {
                                if (err) throw err;
                                return done(null, newUser);
                            });

                        }
                    })
            });
        }
    ));

    ///////////////////////

    // passport.use(new GoogleStrategy({
    //     clientID: configAuth.googleAuth.ClientID,
    //     clientSecret: configAuth.googleAuth.ClientSecret,
    //     callbackURL: configAuth.googleAuth.callbackURL,

    //     profileFields: ['id', 'displayName', 'emails', 'first_name', 'middle_name', 'last_name'],

    //     enableProof: true
    // }, function(accessToken, refreshToken, profile, done) {

    //     process.nextTick(function() {
    //         User.findOne({
    //                 'google.id': profile.id
    //             },
    //             function(err, user) {
    //                 global.tokenFBVMUser[profile.id] = accessToken;
    //                 if (err) {
    //                     return done(err);
    //                 }
    //                 if (user) {
    //                     return done(null, user);
    //                 } else {
    //                     var newUser = new User();
    //                     newUser.google.id = profile.id;
    //                     newUser.google.accessToken = accessToken;
    //                     newUser.google.name = profile.name.displayName;
    //                     newUser.google.email = profile.emails[0].value;

    //                     newUser.save(function(err) {
    //                         if (err) {
    //                             throw err;
    //                         }
    //                         return done(null, newUser);
    //                     });

    //                 }
    //             });
    //     });
    // }));


    //////////////////////////
    // passport.use(new FacebookStrategy({
    //     clientID: configAuth.facebookAuth.ClientID,
    //     clientSecret: configAuth.facebookAuth.ClientSecret,
    //     callbackURL: configAuth.facebookAuth.callbackURL,

    //     profileFields: ['id', 'displayName', 'emails', 'first_name', 'middle_name', 'last_name'],

    //     enableProof: true
    // }, function(accessToken, refreshToken, profile, done) {

    //     process.nextTick(function() {
    //         User.findOne({
    //                 'facebook.id': profile.id
    //             },
    //             function(err, user) {
    //                 global.tokenFBVMUser[profile.id] = accessToken;
    //                 if (err) {
    //                     return done(err);
    //                 }
    //                 if (user) {
    //                     return done(null, user);
    //                 } else {
    //                     var newUser = new User();
    //                     newUser.facebook.id = profile.id;
    //                     newUser.facebook.token = accessToken;
    //                     try {
    //                         if (profile.emails) {
    //                             if (profile.emails.length > 0)
    //                                 newUser.facebook.email = profile.emails[0].value;
    //                             else
    //                                 newUser.facebook.email = profile.emails.value;
    //                         } else {
    //                             if (profile.email)
    //                                 newUser.facebook.email = profile.email.value;
    //                             else
    //                                 newUser.facebook.email = profile.name.givenName + "@facebook.com";
    //                         }
    //                     } catch (e) {
    //                         if (e)
    //                             newUser.facebook.email = profile.id + "@facebook.com";
    //                     }
    //                     if (profile.name.givenName && profile.name.familyName)
    //                         newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;

    //                     newUser.save(function(err) {
    //                         if (err) {
    //                             throw err;
    //                         }
    //                         return done(null, newUser);
    //                     });

    //                 }
    //             });
    //     });
    // }));
    /*
    passport.use(new FacebookStrategy({
            clientID: configAuth.facebookAuth.ClientID,
            clientSecret: configAuth.facebookAuth.ClientSecret,
            callbackURL: configAuth.facebookAuth.callbackURL
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function() {
                User.findOne({ 'facebook.id': profile.id },
                    function(err, user) {
                        if (err) return done(err);
                        if (user) return done(null, user);
                        else {
                            var newUser = new User();
                            newUser.facebook.id = profile.id;
                            newUser.facebook.token = profile.token;
                            newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
                            newUser.facebook.email = profile.email;
                            newUser.save(function(err) {
                                if (err) throw err;
                                return done(null, newUser);
                            });

                        }
                    })
            });
        }
    ));
*/
    ////////

}