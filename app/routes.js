var User = require('./models/user');

module.exports = function(app, passport) {
    app.get('/', function(request, response) {
        //response.send("Hello World Melick from the break my MAN!!");
        response.render("index.ejs");
    });

    app.get('/login', function(request, response) {
        response.render('login.ejs', { message: request.flash('loginmessage') });
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
    }));




    app.get('/signup', function(request, response) {
        response.render('signup.ejs', { message: request.flash('signupmessage') });
    });

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/',
        failureRedirect: '/signup',
        failureFlash: true
    }));

    app.get('/profile', isLoggedIn, function(request, response) {
        response.render('profile.ejs', { user: request.user });
    });

    // app.post('/signup', function(request, response) {
    //     var newUser = new User();
    //     newUser.local.username = request.body.email;
    //     newUser.local.password = request.body.password;
    //     //response.send("Fuck you ass hole");
    //     newUser.save(function(err) {
    //         if (err)
    //             throw err;
    //     });
    //     response.redirect('/');
    // });


    // app.get('/:username/:password', function(request, response) {
    //     var newUser = new User();
    //     newUser.local.username = request.params.username;
    //     newUser.local.password = request.params.password;
    //     //response.send("Fuck you ass hole");
    //     console.log(newUser.local.username + newUser.local.password);
    //     newUser.save(function(err) {
    //         if (err)
    //             throw err;
    //     });
    //     response.send("Success!");
    // });

    app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));
    app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));


    app.get('/logout', function(request, response) {
        request.logout();
        response.redirect('/');
    });
}


function isLoggedIn(request, response, next) {
    if (request.isAuthenticated()) {
        return next();
    }

    response.redirect('/login');
}